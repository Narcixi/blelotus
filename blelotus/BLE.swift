//
//  BLE.swift
//  blelotus
//
//  Created by Sabato Francesco Longobardi on 25/04/21.
//

import Foundation
import SwiftUI
import CoreBluetooth

class BleManager: NSObject, ObservableObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    var bleState : CBManagerState!
    @Published var connectionStatus: Bool! = false
    
    fileprivate let peripheralUUID : CBUUID = CBUUID(string: "7454CD3B-AEA4-D6D9-D61C-B46074FA3E30")
    
    fileprivate var centralManager: CBCentralManager = CBCentralManager()
    fileprivate var peripheral: CBPeripheral?
    fileprivate var serviceUUID: CBUUID!
    //fileprivate var characteristicUUID: CBUUID!
    fileprivate var characteristic: CBCharacteristic!
    
    
    override init() {
        super.init()
        centralManager.delegate = self
        bleConnect()
    }
    
    //functions
    ///will scan and connect
    func bleConnect(){
        if bleState == .poweredOn{
            centralManager.scanForPeripherals(withServices: nil)
        }
        
    }
    
    func bleDisconnect(){
        if peripheral != nil{
            centralManager.cancelPeripheralConnection(self.peripheral!)
            self.peripheral = nil
            connectionStatus = false
        }
    }
    
    func isConnected()->Bool{
        return connectionStatus
    }
    
    func writeValue(data: Data){
        peripheral?.writeValue(data, for: characteristic, type: .withoutResponse)
    }
    
    func writeValue(string: String){
        if let data = string.hexadecimal{
        //    self.writeValue(data: data)
            peripheral?.writeValue(data, for: characteristic, type: .withoutResponse)
        }
    }

    
    //Delegate functions
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        bleState = central.state
        if bleState == .poweredOn{
            //centralManager.scanForPeripherals(withServices: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        self.peripheral = peripheral
        peripheral.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if RSSI.intValue <= -90{
            return
        }
        //print(peripheral.identifier.uuidString)
        if peripheral.identifier.uuidString == peripheralUUID.uuidString{
            peripheral.delegate = self
            self.peripheral = peripheral
            central.connect(peripheral, options: nil)
            central.stopScan()
        }
    }
    
    //peripheral delegate functions
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for serviceIndex in 0..<peripheral.services!.count{
            peripheral.discoverCharacteristics(nil, for: peripheral.services![serviceIndex])
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for charIndex in 0..<service.characteristics!.count{
            if service.characteristics![charIndex].uuid.uuidString == "FFF3"{
                self.characteristic = service.characteristics![charIndex]
                self.connectionStatus = true
                print("connection status: ok")
            }
        }
    }
    
    func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        self.connectionStatus = false
    }
}

extension String {

    /// Create `Data` from hexadecimal string representation
    ///
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.

    var hexadecimal: Data? {
        var data = Data(capacity: self.count / 2)

        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            //print(num)
            data.append(num)
        }

        guard data.count > 0 else { return nil }

        return data
    }

}

extension Color {
    func hexStringFromColor() -> String {
        let components = self.cgColor?.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0

        let hexString = String.init(format: "%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        return hexString
     }
}
