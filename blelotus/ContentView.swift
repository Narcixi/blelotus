//
//  ContentView.swift
//  blelotus
//
//  Created by Sabato Francesco Longobardi on 25/04/21.
//

import SwiftUI

@available(macCatalyst 15.0, *)
struct ContentView: View {
    @ObservedObject var bleManager : BleManager = BleManager()
    let formatter = NumberFormatter()
    @State var brightness: Double = 20
    @State var connected: Bool = false
    @State var color: Color = .white
    @State var breathing: Bool = false
    @State var lightsOn: Bool = true
    
    var body: some View {
        HStack{
            VStack{
                Toggle("Breath", isOn: $breathing).onChange(of: breathing, perform: { isOn in
                    if isOn{
                        breath()
                    }
                })
                Toggle(lightsOn ? "On" : "Off", isOn: .init(get: {
                    lightsOn
                }, set: { newValue in
                    if newValue{
                        bleManager.writeValue(string: "7E0404F00001FF00EF")
                    }else{
                        bleManager.writeValue(string: "7E0404000000FF00EF")
                    }
                    lightsOn = newValue
                }))
                Toggle("", isOn: .constant(false))
                Toggle("", isOn: .constant(false))
                Toggle("", isOn: .constant(false))
            }.padding().fixedSize()
            VStack{
                Circle()
                    .fill(bleManager.connectionStatus ? .green : .red)
                    .frame(width: 100, height: 100, alignment: .center)
                    .onChange(of: bleManager.connectionStatus, perform: {
                        newValue in
                        connected = newValue!
                    })
                Toggle("Connection", isOn: .init(get: {
                    bleManager.isConnected()
                }, set: { newValue in
                    if newValue{
                        bleManager.bleConnect()
                        print("connect")
                    }else{
                        bleManager.bleDisconnect()
                        print("disconnect")
                    }
                }))
                /*.onChange(of: bleManager.connectionStatus, perform: {newValue in
                 connected = newValue
                 })*/
                .padding()
                .frame(width: 200, height: 100, alignment: .center)
                ColorPicker("Color", selection: $color)
                    .frame(width: 500, height: 100, alignment: .center)
                    .onChange(of: color, perform: { newColor in
                        changeColor(hexString: newColor.hexStringFromColor())
                    })
                HStack{
                    Text("Brightness").fixedSize()
                    TextField("Number", value: $brightness, formatter: formatter).fixedSize().padding()
                    Slider(value: $brightness, in: 0...64, step: 1)
                        .frame(width: 300, height: 100, alignment: .center)
                        .padding()
                        .onChange(of: brightness){ brightness in
                            sendBrightness(value: brightness)
                        }
                }
            }
        }
    }
    
    private func sendBrightness(value: Double){
        bleManager.writeValue(string: "7E0401\(Int(value))01FFFF00EF")
    }
    
    private func breath(){
        var times : Double = 0
        _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true){timer in
            times += 4
            if times >= 130 {
                //timer.invalidate()
                times = 0
            }else if times <= 65{
                brightness = times
            }else{
                brightness = 130 - times
            }
            if !breathing{
                timer.invalidate()
            }
        }
    }
    
    private func changeColor(hexString: String){
        bleManager.writeValue(string: "7E070503" + hexString + "10EF")
    }
}

@available(macCatalyst 15.0, *)
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
