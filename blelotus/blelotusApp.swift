//
//  blelotusApp.swift
//  blelotus
//
//  Created by Sabato Francesco Longobardi on 25/04/21.
//

import SwiftUI

@available(macCatalyst 15.0, *)
@main
struct blelotusApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
